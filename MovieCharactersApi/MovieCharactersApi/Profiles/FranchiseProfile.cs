﻿using AutoMapper;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.FranchiseDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
        }
    }
}
