﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.CharacterDTO;
using MovieCharactersApi.Models.DTOs.MovieDTO;
using MovieCharactersApi.Service.IService;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieCharactersApi.Controllers
{
    [ApiController]
    [Route("movies")]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;
        public MovieController(IMovieService movieService, ICharacterService characterService, IMapper mapper)
        {
            _movieService = movieService;
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Retrieve all movies from database.
        /// </summary>
        /// <remarks>Returns empty list if no movies exist</remarks>
        /// <returns>Enumerable of MoviesReadDTO's</returns>
        [SwaggerResponse(200, "Retrieved all movies")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return Ok(_mapper.Map<IEnumerable<MovieReadDTO>>(await _movieService.GetAllAsync()));
        }

        /// <summary>
        /// Add a movie to database.
        /// </summary>
        /// <remarks>
        /// - Title max length is 200 characters.
        /// - Genre max length is 100 characters.
        /// - **ReleaseYear has to be exactly 4 characters long**.
        /// - Director max length is 100 charactes.
        /// ### **To assign a franchise to a movie use the franchise controller**
        /// </remarks>
        /// <param name="movie">Movie to add</param>
        /// <returns>The movie added</returns>
        /// <exception cref="DBConcurrencyException">Could not add movie to database. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(201, "Movie created")]
        [SwaggerResponse(400, "Invalid string length. Check string length requirements!")]
        [SwaggerResponse(409, "Could not add movie to database!")]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie([FromBody] MovieCreateDTO movie)
        {
            var domainMovie = _mapper.Map<Movie>(movie);
            try
            {
                domainMovie = await _movieService.AddAsync(domainMovie);

                return CreatedAtAction("GetMovie",
                    new { id = domainMovie.Id },
                    _mapper.Map<MovieCreateDTO>(domainMovie));
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get a specific movie from database.
        /// </summary>
        /// <param name="id">Movie id</param>
        /// <returns>The movie</returns>
        [SwaggerResponse(200, "Found the movie")]
        [SwaggerResponse(404, "Could not find a movie matching the id provided")]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var Movie = await _movieService.GetSpecificAsync(id);
            if (Movie == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<MovieReadDTO>(Movie));
        }

        /// <summary>
        /// Update a movie
        /// </summary>
        /// <remarks>
        /// - Title max length is 200 characters.
        /// - Genre max length is 100 characters.
        /// - **ReleaseYear has to be exactly 4 characters long**.
        /// - Director max length is 100 charactes.
        /// </remarks>
        /// <param name="id">Movie id</param>
        /// <param name="movie">Movie with fields updated</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not update the movie. Returns 409 Conflict</exception>
        /// <exception cref="DbUpdateException">Invalid string length. Check string length requirements! Returns 400 Bad Request</exception>
        [SwaggerResponse(204, "The movie was updated")]
        [SwaggerResponse(400, "Invalid string length. Check string length requirements!")]
        [SwaggerResponse(404, "The movie does not exist")]
        [SwaggerResponse(409, "Could not update the movie!")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (!_movieService.Exists(id)) return NotFound();

            var domainMovie = _mapper.Map<Movie>(movie);
            domainMovie.Id = id;

            try 
            {
            await _movieService.UpdateAsync(domainMovie);
            }
            catch(DBConcurrencyException)
            {
                return Conflict();
            }
            catch (DbUpdateException)
            {
                return BadRequest();
            }
            return NoContent();
        }

        /// <summary>
        /// Remove a movie
        /// </summary>
        /// <param name="id">movie Id</param>
        /// <returns></returns>
        /// <exception cref="DBConcurrencyException">Could not delete the movie. Returns 409 Conflict.</exception>
        [SwaggerResponse(204, "The movie was removed")]
        [SwaggerResponse(404, "The movie could not be found")]
        [SwaggerResponse(409, "Could not delete the movie!")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.Exists(id)) return NotFound();
            
            try
            {
                await _movieService.DeleteAsync(id);
            }
            catch (DBConcurrencyException)
            {

                return Conflict();
            }
            return NoContent();
        }

        /// <summary>
        /// Get the characters in a movie
        /// </summary>
        /// <remarks>Returns an empty list if no characters exist in movie</remarks>
        /// <param name="id">Movie Id</param>
        /// <returns>List of characters</returns>
        [SwaggerResponse(200, "Retrieved characters")]
        [SwaggerResponse(404, "Movie does not exist")]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            if (!(_movieService.Exists(id))) return NotFound();

            var movieCharacters = await _movieService.GetCharactersInMovie(id);

            return Ok(_mapper.Map<IEnumerable<CharacterReadDTO>>(movieCharacters));
        }

        /// <summary>
        /// Add a character to an existing movie
        /// </summary>
        /// <param name="movieid">Movie id</param>
        /// <param name="characterid">Character id</param>
        /// <returns>A list of all the character in the movie with the added character id updated</returns>
        /// <exception cref="DBConcurrencyException">Could not add the character to the movie. Returns 409 Conflict</exception>
        [SwaggerResponse(201, "Movie has been updated with the new character")]
        [SwaggerResponse(204, "Character already exists in that movie")]
        [SwaggerResponse(404, "Character or movie does not exist")]
        [SwaggerResponse(409, "Could not add character to movie")]
        [HttpPost("movie/{movieid}/characters/{characterid}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> AddCharacterToMovie(int characterid, int movieid)
        {
            if (!_characterService.Exists(characterid) || !_movieService.Exists(movieid)) return NotFound();


            var domainMovie = await _movieService.GetSpecificAsync(movieid);
            var domainCharacter = await _characterService.GetSpecificAsync(characterid);

            if (domainMovie.Characters.Where(c => c.Id == characterid).Count() == 1) return NoContent();
            var characters = domainMovie.Characters;
            characters.Add(domainCharacter);
            try
            {
                await _movieService.UpdateAsync(domainMovie);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }

            return Ok(_mapper.Map<IEnumerable<CharacterReadDTO>>(characters));
        }

        /// <summary>
        /// Removes character from a movie
        /// </summary>
        /// <param name="movieid">Movie id</param>
        /// <param name="characterid">Character id</param>
        /// <returns>List of updated the movie characters without the removed character</returns>
        /// <exception cref="DBConcurrencyException">Could not remove the character from the movie. Returns 409 Conflict</exception>
        [SwaggerResponse(200, "Character id has been removed from the movie")]
        [SwaggerResponse(204, "Character is not in that movie")]
        [SwaggerResponse(404, "Character or movie does not exist")]
        [SwaggerResponse(409, "Could not remove the character from the movie!")]
        [HttpDelete("movie/{movieid}/characters/{characterid}")]

        public async Task<ActionResult<MovieReadDTO>> RemoveCharacterFromMovie(int movieid, int characterid)
        {
            if (!_characterService.Exists(characterid) || !_movieService.Exists(movieid)) return NotFound();

            var domainMovie = await _movieService.GetSpecificAsync(movieid);
            var domainCharacter = await _characterService.GetSpecificAsync(characterid);

            if (domainMovie.Characters.Where(c => c.Id == characterid).Count() == 0) return NoContent();

            var characters = domainMovie.Characters;
            characters.Remove(domainCharacter);
            domainMovie.Characters = characters;
            try
            {
                await _movieService.UpdateAsync(domainMovie);
            }
            catch (DBConcurrencyException)
            {
                return Conflict();
            }

            return Ok(_mapper.Map<IEnumerable<CharacterReadDTO>>(characters));
        }
    }
}
