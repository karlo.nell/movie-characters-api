﻿using MovieCharacters.Model;
using MovieCharactersApi.Models.DTOs.MovieDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Models.DTOs.FranchiseDTO
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
