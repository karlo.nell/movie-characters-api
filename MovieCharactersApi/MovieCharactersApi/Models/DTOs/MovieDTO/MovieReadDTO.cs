﻿using MovieCharacters.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersApi.Models.DTOs.MovieDTO
{
    public class MovieReadDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Trailer { get; set; }
        //Foreign Key
        public int FranchiseId { get; set; }

    }
}
